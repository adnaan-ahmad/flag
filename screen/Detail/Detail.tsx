import React, { useState } from 'react'
import { SafeAreaView, Image } from 'react-native'
import { Snackbar, Button, DataTable } from 'react-native-paper'

import { WEATHER_API, API_KEY } from '../../config/env.json'
import styles from './DetailStyle'

export default function Detail({ route, navigation }: { route: any, navigation: any }) {

    const [loading, setLoading] = useState(false)
    const [visible, setVisible] = useState(false)

    const onDismissSnackBar = () => setVisible(false)

    const handleSubmit = () => {
        setLoading(true)
        fetch(WEATHER_API + API_KEY + '&query=' + route.params.capital)
            .then(data => data.json())
            .then(value => {
                navigation.navigate('Weather', { temperature: value.current.temperature, weather_icons: value.current.weather_icons[0], wind_speed: value.current.wind_speed, precip: value.current.precip })
                setLoading(false)
            })
            .catch(err => {
                setLoading(false)
                setVisible(true)
            })
    }

    return (
        <SafeAreaView style={styles.container}>

            <DataTable style={{ width: '80%' }}>
                <DataTable.Header>
                    <DataTable.Title>Parameter</DataTable.Title>
                    <DataTable.Title numeric>Value</DataTable.Title>
                </DataTable.Header>

                <DataTable.Row>
                    <DataTable.Cell>Capital</DataTable.Cell>
                    <DataTable.Cell numeric>{route.params.capital}</DataTable.Cell>
                </DataTable.Row>

                <DataTable.Row>
                    <DataTable.Cell>Population</DataTable.Cell>
                    <DataTable.Cell numeric>{route.params.population}</DataTable.Cell>
                </DataTable.Row>

                <DataTable.Row>
                    <DataTable.Cell>Latitude</DataTable.Cell>
                    <DataTable.Cell numeric>{route.params.latlng[0]}</DataTable.Cell>
                </DataTable.Row>

                <DataTable.Row>
                    <DataTable.Cell>Longitude</DataTable.Cell>
                    <DataTable.Cell numeric>{route.params.latlng[1]}</DataTable.Cell>
                </DataTable.Row>

                <DataTable.Row style={{ height: 100, justifyContent: 'center' }}>
                    <DataTable.Cell>Flag</DataTable.Cell>
                    <Image
                        style={{ height: 40, width: 40, borderRadius: 20, marginTop: 30 }}
                        source={{ uri: route.params.flag }}
                    />
                </DataTable.Row>
            </DataTable>

            <Button
                loading={loading}
                mode="contained"
                onPress={() => handleSubmit()}
                style={{ marginTop: 20 }}
                color='grey'
                labelStyle={{ color: 'white' }}
            >
                Capital Weather
            </Button>

            <Snackbar
                visible={visible}
                onDismiss={onDismissSnackBar}
                action={{
                    label: 'OK',
                    onPress: () => {
                        setVisible(false)
                    },
                }}>
                {'Unable to display weather for ' + route.params.capital}
            </Snackbar>

        </SafeAreaView>
    )
}
