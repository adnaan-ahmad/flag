import React, { useState } from 'react'
import { SafeAreaView } from 'react-native'
import { Snackbar, Button, TextInput } from 'react-native-paper'

import { COUNTRY_API } from '../../config/env.json'
import styles from './HomeStyle'

export default function Home({ navigation }: { navigation: any }) {

    const [country, setCountry] = useState('')
    const [loading, setLoading] = useState(false)
    const [visible, setVisible] = useState(false)

    const onDismissSnackBar = () => setVisible(false)

    const handleSubmit = () => {
        setLoading(true)
        fetch(COUNTRY_API + country)
            .then(data => data.json())
            .then(value => {
                navigation.navigate('Detail', { capital: value[0].capital[0], population: value[0].population, latlng: value[0].latlng, flag: value[0].flags.png })
                setLoading(false)
            })
            .catch(err => {
                setLoading(false)
                setVisible(true)
            })
    }

    return (
        <SafeAreaView style={styles.container}>

            <TextInput
                label="Enter Country"
                value={country}
                style={styles.textInput}
                onChangeText={(value) => setCountry(value)}
                mode='outlined'
                activeOutlineColor='grey'
            />

            <Button
                loading={loading}
                mode="contained"
                onPress={() => handleSubmit()}
                style={{ marginTop: 20 }}
                disabled={country.trim() === '' ? true : false}
                color='grey'
                labelStyle={{ color: 'white' }}
            >
                Submit
            </Button>

            <Snackbar
                visible={visible}
                onDismiss={onDismissSnackBar}
                action={{
                    label: 'OK',
                    onPress: () => {
                        setVisible(false)
                    },
                }}>
                {'Information not available for ' + country}
            </Snackbar>

        </SafeAreaView>
    )
}
