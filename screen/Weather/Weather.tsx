import React from 'react'
import { SafeAreaView, Image } from 'react-native'
import { DataTable } from 'react-native-paper'

import styles from './WeatherStyle'

export default function Weather({ route }: { route: any }) {

    return (
        <SafeAreaView style={styles.container}>

            <DataTable style={{ marginHorizontal: 20, width: '80%' }}>
                <DataTable.Header>
                    <DataTable.Title>Parameter</DataTable.Title>
                    <DataTable.Title numeric>Value</DataTable.Title>
                </DataTable.Header>

                <DataTable.Row>
                    <DataTable.Cell>Temperature</DataTable.Cell>
                    <DataTable.Cell numeric>{route.params.temperature}</DataTable.Cell>
                </DataTable.Row>

                <DataTable.Row>
                    <DataTable.Cell>Wind Speed</DataTable.Cell>
                    <DataTable.Cell numeric>{route.params.wind_speed}</DataTable.Cell>
                </DataTable.Row>

                <DataTable.Row>
                    <DataTable.Cell>Precipitation</DataTable.Cell>
                    <DataTable.Cell numeric>{route.params.precip}</DataTable.Cell>
                </DataTable.Row>

                <DataTable.Row style={{ height: 100, justifyContent: 'center' }}>
                    <DataTable.Cell>Weather Icon</DataTable.Cell>
                    <Image
                        style={{ height: 40, width: 40, borderRadius: 20, marginTop: 30 }}
                        source={{ uri: route.params.weather_icons }}
                    />
                </DataTable.Row>
            </DataTable>

        </SafeAreaView>
    )
}
